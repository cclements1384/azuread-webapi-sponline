﻿using System;
using Domain.Services;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace Repository
{
    public class SPOTokenProvider : ISPOTokenService
    {
        private readonly string _spoUrl;
        private readonly string _clientId;
        private readonly string _clientSecret;

        public SPOTokenProvider(string spoUrl,
            string clientId,
            string clientSecret)
        {
            _spoUrl = spoUrl;
            _clientId = clientId;
            _clientSecret = clientSecret;
        }

        public string GetAccessToken(string accessToken)
        {
            string clientID = _clientId;            //ConfigurationManager.AppSettings["ClientID"];
            string clientSecret = _clientSecret;    // ConfigurationManager.AppSettings["ClientSecret"];

            var appCred = new ClientCredential(clientID, clientSecret);
            var authContext = new Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext("https://login.windows.net/common");

            AuthenticationResult authResult = authContext.AcquireToken(new Uri(_spoUrl).GetLeftPart(UriPartial.Authority), appCred, new UserAssertion(accessToken));
            return authResult.AccessToken;
        }

     
    }
}
