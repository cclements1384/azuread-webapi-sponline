﻿using Domain.Models;
using Microsoft.SharePoint.Client;
using System.Collections.Generic;

namespace Repository
{
    public class DocumentModelMapper
    {
        public static List<DocumentModel> MapAll(ListItemCollection listItems)
        {
            List<DocumentModel> documents = new List<DocumentModel>();
            foreach (var item in listItems)
            {
                documents.Add(MapSingle(item));
            }
            return documents;
        }

        public static DocumentModel MapSingle(ListItem item)
        {
            var name = "";
            if (item["FileLeafRef"] != null)
                name = item["FileLeafRef"].ToString();

            var cruId = "";
            if (item["CRUID"] != null)
                cruId = item["CRUID"].ToString();

            return new DocumentModel
            {
                Name = name,
                CRUID = cruId
            };
        }
    }
}
