﻿using Microsoft.SharePoint.Client;

namespace Repository
{
    public class CamlQueryProvider
    {
        public static CamlQuery GetById(string id)
        {
            var camlQuery = new CamlQuery();
            camlQuery.ViewXml = string.Format(@"<View Scope='Recursive'>
                                                        <Query>
                                                            <Where>
                                                                <Eq>
                                                                    <FieldRef Name='CRUID'/>
                                                                    <Value Type='Text'>{0}</Value>
                                                                </Eq>
                                                            </Where>
                                                        </Query>
                                                        <RowLimit>100</RowLimit>
                                                    </View>",
                                                id);
            return camlQuery;
        }

        public static CamlQuery GetAll()
        {
            var camlQuery = new CamlQuery();
            camlQuery.ViewXml = @"<View Scope='Recursive'>
                                                        <Query>
                                                        </Query>
                                                        <RowLimit>100</RowLimit>
                                                    </View>";

            return camlQuery;
        }
    }
}
