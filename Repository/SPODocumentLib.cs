﻿using Domain.Services;
using System;
using System.Collections.Generic;
using Domain.Models;
using Microsoft.SharePoint.Client;

namespace Repository
{
    public class SPODocumentLib : ICRUDocumentRepository
    {
        private readonly ISPOTokenService _tokenSvc;
        private readonly string _folderName;
        private readonly string _libraryName;
        private readonly string _spoUrl;

        public SPODocumentLib(ISPOTokenService tokenSvc,
             string spoUrl,
             string libraryName,
             string folderName)
        {
            _tokenSvc = tokenSvc;
            _libraryName = libraryName;
            _folderName = folderName;
            _spoUrl = spoUrl;
        }
        
        public IEnumerable<DocumentModel> Get(string accessToken)
        {
            var query = CamlQueryProvider.GetAll();
            return ListDocuments(accessToken, query);
        }

        public IEnumerable<DocumentModel> GetById(string accessToken, string cruId)
        {
            var query = CamlQueryProvider.GetById(cruId);
            return ListDocuments(accessToken, query);
        }
    
        public void Post(string accessToken)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<DocumentModel> ListDocuments(string accessToken, CamlQuery camlQuery)
        {
            string response = string.Empty;
            string newToken = _tokenSvc.GetAccessToken(accessToken);
            List<DocumentModel> documents = new List<DocumentModel>();

            /* Beginning CSOM Magic */
            using (ClientContext cli = new ClientContext(_spoUrl))
            {
                /* Adding authorization header  */
                cli.ExecutingWebRequest += (s, e) => e.WebRequestExecutor.WebRequest.Headers.Add("Authorization", "Bearer " + newToken);
              
                Web web = cli.Web;
                List DocumentsList = cli.Web.Lists.GetByTitle(_libraryName);
                ListItemCollection listItems = DocumentsList.GetItems(camlQuery);
                cli.Load(listItems);
                cli.ExecuteQuery();

                /* Map to DocumentModels */
                documents = DocumentModelMapper.MapAll(listItems);
               
            }
            return documents;
        }
        
    }
}
