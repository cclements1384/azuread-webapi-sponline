﻿using Domain.Models;
using Repository;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;

namespace WebAPI.Controllers
{
    //[Authorize]
    public class DocumentsController : ApiController
    {
        [HttpGet]
        public IEnumerable<DocumentModel> Get()
        {
            var spoUrl = "https://cop.sharepoint.com";
            /* these values are for the spoWebApi applicatiin in cop.onmicrosoft.com */
            var clientId = ConfigurationManager.AppSettings["ClientID"];
            var clientSecret = ConfigurationManager.AppSettings["ClientSecret"];
            var tokenSvc = new SPOTokenProvider(spoUrl, clientId, clientSecret);
            

            var libraryName = "Documents";
            var folderName = "f001";
            var repo = new SPODocumentLib(tokenSvc, spoUrl,libraryName,folderName);

            var accessToken = this.Request.Headers.Authorization.Parameter;
            return repo.Get(accessToken);

        }

        //internal static string GetSharePointAccessToken(string url, string accessToken)
        //{
        //    string clientID = ConfigurationManager.AppSettings["ClientID"];
        //    string clientSecret = ConfigurationManager.AppSettings["ClientSecret"];

        //    var appCred = new ClientCredential(clientID, clientSecret);
        //    var authContext = new Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext("https://login.windows.net/common");

        //    AuthenticationResult authResult = authContext.AcquireToken(new Uri(url).GetLeftPart(UriPartial.Authority), appCred, new UserAssertion(accessToken));
        //    return authResult.AccessToken;
        //}

        //private IEnumerable<DocumentModel> ListDocuments(string libraryName, string folderName)
        //{
        //    string response = string.Empty;


        //    string sharePointUrl = ConfigurationManager.AppSettings["SharePointURL"];
        //    string newToken = GetSharePointAccessToken(sharePointUrl, this.Request.Headers.Authorization.Parameter);

        //    using (ClientContext cli = new ClientContext(sharePointUrl))
        //    {
        //        FileCollection files = null;
        //        List<DocumentModel> documents = new List<DocumentModel>();


        //        / Adding authorization header
        //        cli.ExecutingWebRequest += (s, e) => e.WebRequestExecutor.WebRequest.Headers.Add("Authorization", "Bearer " + newToken);


        //        Web web = cli.Web;
        //        ListCollection lists = web.Lists;

        //        var docLib = web.Lists.GetByTitle(libraryName);

        //        =======
        //        List DocumentsList = cli.Web.Lists.GetByTitle(libraryName);
        //        CamlQuery camlQuery = new CamlQuery();
        //        camlQuery = new CamlQuery();
        //        camlQuery.ViewXml = @"<View Scope='Recursive'>
        //                                <Query>
        //                                    <Where>
        //                                        <Eq>
        //                                            <FieldRef Name='CRUID'/>
        //                                            <Value Type='Text'>CRU_001</Value>
        //                                        </Eq>
        //                                    </Where>
        //                                </Query>
        //                                <RowLimit>100</RowLimit>
        //                            </View>";
        //        ListItemCollection listItems = DocumentsList.GetItems(camlQuery);
        //        cli.Load(listItems);
        //        cli.ExecuteQuery();

        //        foreach (var item in listItems)
        //        {
        //            var document = new DocumentModel
        //            {
        //                Name = item["FileLeafRef"].ToString(),
        //                CRUID = "0001",

        //            };
        //            documents.Add(document);
        //        }





        //        ============



        //        cli.Load(docLib,
        //                           d => d.Title,
        //                           d => d.RootFolder.Name);
        //        cli.ExecuteQuery();

        //        String folderUrl = String.Format("/{0}/{1}", docLib.RootFolder.Name, folderName);

        //        var folders = docLib.RootFolder.Folders;
        //        cli.Load(folders, fldrs => fldrs.Include(fldr => fldr.ServerRelativeUrl));
        //        cli.ExecuteQuery();

        //        var folder = folders.FirstOrDefault(f => f.ServerRelativeUrl.ToLower() == folderUrl.ToLower());
        //        if (folder != null)
        //        {
        //            files = folder.Files;


        //            CamlQuery camlQuery = new CamlQuery();
        //            camlQuery.ViewXml = "<View><Query><Where><eq><FieldRef Name='CRUID'/><Value Type='Text'>CRU_001</Value></Geq></Where></Query><RowLimit>100</RowLimit></View>";



        //            cli.Load(files, fls => fls.Include(fl => fl.Name));
        //            cli.ExecuteQuery();

        //            foreach (var file in files)
        //            {
        //                var document = new DocumentModel
        //                {
        //                    Name = file.Name,
        //                    CRUID = "0001",

        //                };
        //                documents.Add(document);
        //            }
        //        }

        //        return documents;

        //    }
        }
}