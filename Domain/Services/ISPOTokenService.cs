﻿namespace Domain.Services
{
    public interface ISPOTokenService
    {
        string GetAccessToken(string accessToken);
    }
}
