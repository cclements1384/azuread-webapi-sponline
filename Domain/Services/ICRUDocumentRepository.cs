﻿using Domain.Models;
using System.Collections.Generic;

namespace Domain.Services
{
    public interface ICRUDocumentRepository
    {
        IEnumerable<DocumentModel> Get(string accessToken);
        IEnumerable<DocumentModel> GetById(string accessToken, string cruId);
        void Post(string accessToken);
    }
}
