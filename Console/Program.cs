﻿using Domain.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SPOConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            /* I registered the applications in the cop.onmicrosoft.com domain.
             * The user is cclements@cop.onmicrosoft.com
             * The API app is spoWebAPI (permissioned to SPO)  
             * The client is spoWebAPIClient (permissioned to spoWebAPI)
             */
          
            /// Azure AD WebApi's APP ID URL 
            string resource = "https://cop.onmicrosoft.com/spoWebAPI";

            /// Azure AD WebApi's Client ID  (registed as spiWebAPIClient in Conocophillips (cop.onmicrosoft.com)
            string clientId = "1a1e96e1-fff5-4f5d-b78d-dfaa653c3fa5";

            /// Azure AD (cop.onmicrosoft.com) User's credentials 
            string userName = "cclements@cop.onmicrosoft.com";
            string userPassword = "Passwo11";
            
            // get the user credential
            var user = new UserCredential(userName, userPassword);

            // Get the Auth context
            //var authContext = new AuthenticationContext("https://login.windows.net/common"); //this enables multi-tenant lookup of users.
            var authContext = new AuthenticationContext("https://login.windows.net/cop.onmicrosoft.com"); //single tenant cop.onmicrosoft.com in this case.


            /// Get an Access Token to Access the Web API on behalf of the user 
            AuthenticationResult authResult = 
                authContext.AcquireTokenAsync(resource, clientId, user).Result;

            /// Call WebAPI passing Access token on header 
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = 
                new AuthenticationHeaderValue("Bearer", authResult.AccessToken);

            Console.Write("Waiting for API to come online.  Press anykey to call API.");
            Console.ReadLine();

            /// Get the result  
            string apiUrl = "http://localhost:14983/api/Documents";
            HttpResponseMessage response = client.GetAsync(apiUrl).Result;
            string text = response.Content.ReadAsStringAsync().Result;
            List<DocumentModel> documents = JsonConvert.DeserializeObject<List<DocumentModel>>(text);

            // Display files from Library
            Console.WriteLine("");
            Console.WriteLine("Listing files in document library");
            foreach(var doc in documents)
            {
                Console.WriteLine("{0} {1}", doc.Name, doc.CRUID);
            }

            //apiUrl = "http://localhost:14983/api/Documents/GetById";
            //response = client.GetAsync(apiUrl).Result;
            //text = response.Content.ReadAsStringAsync().Result;
            //documents = JsonConvert.DeserializeObject<List<DocumentModel>>(text);


            Console.Read();

        }
    }
}
